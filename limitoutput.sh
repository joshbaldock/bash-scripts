#!/bin/bash
# This is a wrapper script to limit the output lines of a command. Typically this script
# would be used with cron, so as to limit the amount of output of a cronjob command.
#
# Script:: limitoutput.sh
# Author:: Joshua Baldock
# Date Created:: 07/08/13
# Usage:: limitoutput.sh NUMLINES COMMAND [ARGS]
#
# Date Updated:: 03/09/13
# - Bugfix: Resolved argument passing issue with external script

# limit output function
limit_output() {
	c=0
	while read data; do
		# Outputs lines of data
		if [ $c -lt $numlines ]; then 
			echo $data
		fi
		# Check if number of required lines has been reached
		if [ $c -eq $numlines ]; then
			echo ">>> Output limited to ${numlines} lines <<<"
		fi
		let c++	
	done
}

# Check for required arguments
if ! [ $# -ge 2 ]; then
	echo "This script will limit the ouput of a given command to a given number of lines"
	echo ""
	echo "Usage: $0 NUMLINES COMMAND [ARGS]"
	exit 0
else
	args=("$@")
fi

# Set number of lines variable
numlines=${args[0]}
unset args[0]

# Check numlines is an integer
if ! [[ "$numlines" =~ ^[0-9]*$ ]]; then
	echo "Error NUMLINES must be an interger"
	exit 1
fi

# Set command to run variable
command="${args[1]}"
unset args[1]

# Check command is found
command -v $command >/dev/null 2>&1 || { echo "Unable to find command ${command}"; exit 1; }

# Set command arguments to variable
args="${args[@]}"

# Output what will run
echo "Executing: $command $args"

# Execute supplied command with arguments and pass through limit output function. $args quoted to assist with argument passing to external scripts.
(
$command "$args"
) 2>&1 | limit_output
